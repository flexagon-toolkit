#!/usr/bin/env python

from affine import Affine
from math import sin, cos, radians

src_x = 10
src_y = 10

angle = 30

dest_x = 20
dest_y = 30

# Some transform to compose
T1 = Affine.translation(-src_x, -src_y)
R = Affine.rotation(angle)
T2 = Affine.translation(dest_x, dest_y)

# Composition is performed by multiplying from right to left
matrix = T2 * R * T1
print [item for item in matrix]

theta = radians(angle)

# This is the equivalent transformation matrix
matrix = [
    cos(theta), -sin(theta), -src_x * cos(theta) + src_y * sin(theta) + dest_x,
    sin(theta),  cos(theta), -src_x * sin(theta) - src_y * cos(theta) + dest_y,
           0.0,                              0.0,                          1.0
]
print matrix
