#!/usr/bin/env python
#
# Draw a hexaflexagon template with cairo.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from diagram.cairo_diagram import CairoDiagram
from flexagon import HexaflexagonDiagram


def draw_cairo_template():
    # A4 page at 300dpi is 3508x2480 in pixels but cairo expects units to be in
    # points, so adjust the values.
    width = 3508 / 1.25
    height = 2480 / 1.25

    x_border = width / 50
    font_size = width / 80
    stroke_width = width / 480

    cairo_backend = CairoDiagram(width, height, font_size=font_size, stroke_width=stroke_width)
    hexaflexagon = HexaflexagonDiagram(x_border, backend=cairo_backend)

    cairo_backend.clear()
    hexaflexagon.draw_template()
    cairo_backend.save_svg('hexaflexagon-template')


if __name__ == '__main__':
    draw_cairo_template()
