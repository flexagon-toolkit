#!/usr/bin/env python

'''
Gimp plugin "Hexaflexagon"

Create Hexaflexagons.

Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

# The plugin is inspired to flexagon.scm by Andrea Rossetti:
# http://andrear.altervista.org/home/gimp_flexagon.php
#
# It has been rewritten in python in the hope to simplify it and attract more
# contributors.

from gimpfu import *

from diagram.gimp_diagram import GimpDiagram
from flexagon import HexaflexagonDiagram

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)


def build_plan(template_layer, hexaflexagon_layer, diagram):
    for hexagon in diagram.hexaflexagon.hexagons:
        for triangle in hexagon.triangles:
            verts = diagram.get_triangle_verts(triangle)

            matrix = diagram.get_triangle_transform(triangle)
            diagram.backend.copy_polygon(template_layer, verts, hexaflexagon_layer, matrix)


def hexaflexagon_main(image):
    x_border = image.width / 50
    font_size = image.width / 80
    stroke_width = image.width / 480

    template_layer_name = "HexaflexagonTemplate"
    content_layer_name = "Hexagons"
    hexaflexagon_layer_name = "Hexaflexagon"

    message = "Draw the hexagons content into the '%s' layer.\n" % content_layer_name
    message += "Then call this script again."

    pdb.gimp_image_undo_group_start(image)
    pdb.gimp_context_push()
    pdb.gimp_context_set_defaults()

    template_layer = pdb.gimp_image_get_layer_by_name(image,
                                                      template_layer_name)

    content_layer = pdb.gimp_image_get_layer_by_name(image,
                                                     content_layer_name)
    if not content_layer:
        content_layer = pdb.gimp_layer_new(image, image.width, image.height,
                                           RGBA_IMAGE, content_layer_name,
                                           100, NORMAL_MODE)
        if template_layer:
            template_layer_position = pdb.gimp_image_get_item_position(image, template_layer)
            content_layer_position = template_layer_position - 1
            pdb.gimp_image_insert_layer(image, content_layer, None, content_layer_position)

            pdb.gimp_message(message)

            pdb.gimp_context_pop()
            pdb.gimp_image_undo_group_end(image)
            return
        else:
            content_layer_position = -1
            pdb.gimp_image_insert_layer(image, content_layer, None, content_layer_position)

    if not template_layer:
        template_layer = pdb.gimp_layer_new(image, image.width, image.height,
                                            RGBA_IMAGE, template_layer_name,
                                            100, NORMAL_MODE)
        pdb.gimp_image_insert_layer(image, template_layer, None, -1)

        gimp_backend = GimpDiagram(image.width, image.height,
                                   image, template_layer,
                                   font_size=font_size, stroke_width=stroke_width)
        diagram = HexaflexagonDiagram(x_border, backend=gimp_backend)
        diagram.draw_template()

        pdb.gimp_message(message)
        pdb.gimp_context_pop()
        pdb.gimp_image_undo_group_end(image)
        return

    hexaflexagon_layer = pdb.gimp_image_get_layer_by_name(image,
                                                          hexaflexagon_layer_name)
    if hexaflexagon_layer:
        pdb.gimp_message("There is already a generated hexaflexagon.")
        pdb.gimp_context_pop()
        pdb.gimp_image_undo_group_end(image)
        return

    hexaflexagon_layer = pdb.gimp_layer_new(image, image.width,
                                            image.height, RGBA_IMAGE,
                                            hexaflexagon_layer_name, 100,
                                            NORMAL_MODE)
    pdb.gimp_image_insert_layer(image, hexaflexagon_layer, None, -1)

    gimp_backend = GimpDiagram(image.width, image.height,
                               image, content_layer,
                               font_size=font_size, stroke_width=stroke_width)
    diagram = HexaflexagonDiagram(x_border, backend=gimp_backend)
    build_plan(content_layer, hexaflexagon_layer, diagram)

    pdb.gimp_context_pop()
    pdb.gimp_image_undo_group_end(image)


if __name__ == "__main__":
    register(
        "python_fu_hexaflexagon",
        N_("Create Hexaflexagons"),
        "Create Hexaflexagons",
        "Antonio Ospite <ao2@ao2.it>",
        "Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>",
        "2015",
        N_("Hexaflexagon..."),
        "RGB*, GRAY*",
        [
            (PF_IMAGE, "image", "Input image", None),
        ],
        [],
        hexaflexagon_main,
        menu="<Image>/Filters/Render",
        domain=("gimp20-python", gimp.locale_directory)
    )

    main()
