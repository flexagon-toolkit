#!/usr/bin/env python3
#
# Draw an SVG tetraflexagon which can be edited live in Inkscape.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import svgwrite

from diagram.svgwrite_diagram import SvgwriteDiagram
from flexagon.tetraflexagon_diagram import TetraflexagonDiagram


class SvgwriteTetraflexagonDiagram(TetraflexagonDiagram):
    def __init__(self, *args, **kwargs):
        super(SvgwriteTetraflexagonDiagram, self).__init__(*args, **kwargs)

        svg = self.backend.svg

        # create some layers and groups
        layers = {
            "Squares": svg.layer(label="Squares"),
            "Backfaces": svg.layer(label="Backfaces"),
            "Tetraflexagon": svg.layer(label="Tetraflexagon"),
            "Template": svg.layer(label="Template")
        }
        for layer in layers.values():
            svg.add(layer)

        self.groups = layers

        for square in self.tetraflexagon.squares:
            name = "square%d-content" % square.index
            layer = svg.layer(id=name, label="Square %d" % (square.index + 1))
            self.groups[name] = layer
            layers['Squares'].add(layer)

            for tile in square.tiles:
                name = "square%d-tile%d" % (square.index, tile.index)
                group = svg.g(id=name)
                self.groups[name] = group
                layers['Template'].add(group)

    def draw(self):
        for square in self.tetraflexagon.squares:
            cx, cy = self.get_square_center(square)

            # Draw some default content
            old_active_group = self.backend.active_group
            self.backend.active_group = self.groups["square%d-content" % square.index]
            self.backend.draw_rect_from_center(cx, cy, self.square_side, self.square_side, 0,
                                               fill_color=(0.5, 0.5, 0.5, 0.2))
            self.backend.active_group = old_active_group

        # Draw the normal template for squares
        for square in self.tetraflexagon.squares:
            self.draw_square_template(square)

        # draw plan using references
        for square in self.tetraflexagon.squares:
            for tile in square.tiles:
                m = self.get_tile_transform(tile)
                svg_matrix = "matrix(%f, %f, %f, %f, %f, %f)" % (m[0], m[3],
                                                                 m[1], m[4],
                                                                 m[2], m[5])

                # Reuse the squares tile for the tetraflexagon template
                group = self.groups["Template"]
                tile_href = "#square%d-tile%d" % (square.index, tile.index)
                ref = self.backend.svg.use(tile_href)
                ref['transform'] = svg_matrix
                group.add(ref)

                # Reuse the content to draw the final tetraflexagon
                group = self.groups["Tetraflexagon"]
                content_href = "#square%d-content" % square.index
                ref = self.backend.svg.use(content_href)
                ref['transform'] = svg_matrix
                ref['clip-path'] = "url(%s)" % (tile_href + '-clip-path')
                group.add(ref)

        # A tri-tetraflexagon only have one visible backface, the first one.
        group = self.groups["Backfaces"]
        for tile in self.tetraflexagon.squares[0].tiles:
            m = self.get_backface_tile_transform(tile)
            svg_matrix = "matrix(%f, %f, %f, %f, %f, %f)" % (m[0], m[3],
                                                             m[1], m[4],
                                                             m[2], m[5])
            tile_href = "#square0-tile%d" % tile.index
            ref = self.backend.svg.use("#square0-content")
            ref['transform'] = svg_matrix
            ref['clip-path'] = "url(%s)" % (tile_href + '-clip-path')
            group.add(ref)

    def draw_tile_template(self, tile, cx, cy, theta):
        old_active_group = self.backend.active_group
        group_name = "square%d-tile%d" % (tile.square.index, tile.index)
        self.backend.active_group = self.groups[group_name]

        super(SvgwriteTetraflexagonDiagram, self).draw_tile_template(tile, cx, cy, theta)

        # The tile outline in the active group's element is the only rectangle
        # element, so get it and set its id so that it can be reused as
        # a clip-path
        for element in self.backend.active_group.elements:
            if isinstance(element, svgwrite.shapes.Rect):
                element['id'] = group_name + "-outline"
                break

        clip_path = self.backend.svg.clipPath(id=group_name + '-clip-path')
        self.backend.svg.defs.add(clip_path)
        ref = self.backend.svg.use('#%s-outline' % group_name)
        clip_path.add(ref)

        self.backend.active_group = old_active_group


def main():
    width = 3508
    height = 2480

    x_border = width / 50
    font_size = width / 80
    stroke_width = width / 480

    svg_backend = SvgwriteDiagram(width, height, font_size=font_size, stroke_width=stroke_width)
    tetraflexagon = SvgwriteTetraflexagonDiagram(x_border, backend=svg_backend)
    tetraflexagon.draw()
    svg_backend.save_svg("inkscape-tetraflexagon-editor.svg")


if __name__ == "__main__":
    main()
