#!/usr/bin/env python
#
# A Diagram base class
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from math import cos, sin, pi, fmod


class Diagram(object):
    def __init__(self, width, height, background=(1, 1, 1), font_size=20, stroke_width=2):
        self.width = width
        self.height = height
        self.background = background
        self.font_size = font_size
        self.stroke_width = stroke_width

    def clear(self):
        raise NotImplementedError

    @staticmethod
    def test(diagram):
        diagram.clear()

        x = 40
        y = 200

        x_offset = x

        theta = 0

        diagram.draw_line(0, y, 400, y, (1, 0, 0, 1))

        advance = diagram.draw_centered_text(x_offset, y, "Ciao", theta,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "____", theta + pi / 4,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "jxpqdlf", theta + pi / 2,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "pppp", theta + 3 * pi / 4,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "dddd", theta + pi,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "Jjjj", theta + 5 * pi / 4,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "1369", theta + 3 * pi / 2,
                                             color=(0, 1, 0),
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        advance = diagram.draw_centered_text(x_offset, y, "qqqq", theta + 7 * pi / 4,
                                             align_baseline=True,
                                             bb_stroke_color=(0, 0, 0, 0.5),
                                             bb_fill_color=(1, 1, 1, 0.8))
        x_offset += advance

        diagram.draw_rect(40, 40, 300, 100, stroke_color=(0, 0, 0, 0.8))
        diagram.draw_rect(40, 40, 300, 100, pi / 30, stroke_color=(0, 0, 0, 0.8))

        verts = diagram.draw_regular_polygon(190, 90, 3, 20)

        diagram.draw_rect(40, 250, 300, 100, stroke_color=(0, 0, 0, 0.8))
        diagram.draw_rect_from_center(40 + 150, 250 + 50, 300, 100, theta=(pi / 40),
                                      stroke_color=(1, 0, 0),
                                      fill_color=None)

        verts = diagram.draw_regular_polygon(190, 300, 6, 20, pi / 3., (0, 0, 1, 0.5), (0, 1, 0.5))
        diagram.draw_apothem_star(190, 300, 6, 20, 0, (1, 0, 1))

        diagram.draw_star_by_verts(190, 300, verts, (1, 0, 0, 0.5))
        diagram.draw_star(190, 300, 6, 25, 0, (1, 0, 1, 0.2))

        diagram.draw_circle(190, 300, 30, (0, 1, 0, 0.5), None)
        diagram.draw_circle(100, 300, 30, (1, 0, 0, 0.5), (0, 1, 1, 0.5))

    @staticmethod
    def color_to_rgba(color):
        assert len(color) >= 3

        color = tuple(float(c) for c in color)
        if len(color) == 3:
            color += (1.0,)

        return color

    @staticmethod
    def normalized_angle_01(theta):
        return fmod(theta, 2 * pi) / (2 * pi)

    @staticmethod
    def calc_rotate_translate_transform(src_x, src_y, dest_x, dest_y, theta):
        """Calculate the transformation matrix resulting from a rotation and
        a translation.

        Return the matrix as a list of values sorted in row-major order."""

        # A rotate-translate transformation is composed by these steps:
        #
        #   1. rotate by 'theta' around (src_x, src_y);
        #   2. move to (dest_x, dest_y).
        #
        # Step 1 can be expressed by these sub-steps:
        #
        #  1a. translate by (-src_x, -src_y)
        #  1b. rotate by 'theta'
        #  1c. translate by (src_x, src_y)
        #
        # Step 2. can be expressed by a translation like:
        #
        #  2a. translate by (dest_x - src_x, dest_y - src_y)
        #
        # The consecutive translations 1c and 2a can be easily combined, so
        # the final steps are:
        #
        #  T1 -> translate by (-src_x, -src_y)
        #  R  -> rotate by 'theta'
        #  T2 -> translate by (dest_x, dest_y)
        #
        # Using affine transformations these are expressed as:
        #
        #      | 1  0  -src_x |
        # T1 = | 0  1  -src_y |
        #      | 0  0       1 |
        #
        #      | cos(theta)  -sin(theta)  0 |
        # R  = | sin(theta)   cos(theta)  0 |
        #      |          0            0  1 |
        #
        #      | 1  0  dest_x |
        # T2 = | 0  1  dest_y |
        #      | 0  0       1 |
        #
        # Composing these transformations into one is achieved by multiplying
        # the matrices from right to left:
        #
        #   T = T2 * R * T1
        #
        # NOTE: To remember this think about composing functions: T2(R(T1())),
        # the inner one is performed first.
        #
        # The resulting  T matrix is the one below.
        matrix = [
            cos(theta), -sin(theta), -src_x * cos(theta) + src_y * sin(theta) + dest_x,
            sin(theta),  cos(theta), -src_x * sin(theta) - src_y * cos(theta) + dest_y,
                     0,           0,                                                 1
        ]

        return matrix

    @staticmethod
    def get_regular_polygon(x, y, sides, r, theta0=0.0):
        """Calc the coordinates of the regular polygon.

        NOTE: the first point will be in axis with y."""
        theta = 2 * pi / sides

        verts = []
        for i in range(sides):
            px = x + r * sin(theta0 + i * theta)
            py = y + r * cos(theta0 + i * theta)
            verts.append((px, py))

        return verts

    def draw_polygon_by_verts(self, verts,
                              stroke_color=(0, 0, 0),
                              fill_color=None):
        raise NotImplementedError

    def draw_regular_polygon(self, cx, cy, sides, r, theta=0.0,
                             stroke_color=(0, 0, 0),
                             fill_color=None):
        verts = self.get_regular_polygon(cx, cy, sides, r, theta)
        self.draw_polygon_by_verts(verts, stroke_color, fill_color)
        return verts

    def draw_star_by_verts(self, cx, cy, verts, stroke_color=(0, 0, 0)):
        raise NotImplementedError

    def draw_star(self, cx, cy, sides, r, theta=0.0, stroke_color=(0, 0, 0)):
        verts = self.get_regular_polygon(cx, cy, sides, r, theta)
        self.draw_star_by_verts(cx, cy, verts, stroke_color)
        return verts

    def draw_apothem_star(self, cx, cy, sides, r, theta=0.0, stroke_color=(0, 0, 0)):
        """Draw a star but calculate the regular polygon apothem from the passed radius."""
        apothem = r * cos(pi / sides)
        apothem_angle = theta + pi / sides

        return self.draw_star(cx, cy, sides, apothem, apothem_angle, stroke_color)

    def draw_rect(self, x, y, width, height, theta=0,
                  stroke_color=None,
                  fill_color=(1, 1, 1, 0.8)):
        raise NotImplementedError

    def draw_rect_from_center(self, cx, cy, width, height, theta=0.0,
                              stroke_color=None,
                              fill_color=(1, 1, 1, 0.8)):
        # the position of the center of a rectangle at (0,0)
        mx = width / 2.0
        my = height / 2.0

        # calculate the position of the bottom-left corner after rotating the
        # rectangle around the center
        rx = cx - (mx * cos(theta) - my * sin(theta))
        ry = cy - (mx * sin(theta) + my * cos(theta))

        self.draw_rect(rx, ry, width, height, theta, stroke_color, fill_color)
