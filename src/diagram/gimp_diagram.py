#!/usr/bin/env python
#
# A diagram class to draw in Gimp
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import warnings
from gimpfu import *

from .diagram import Diagram


class GimpDiagram(Diagram):
    def __init__(self, width, height, image, layer, **kwargs):
        super(GimpDiagram, self).__init__(width, height, **kwargs)

        self.image = image
        self.drawable = layer

    def color_to_rgba(self, color):
        color = super(GimpDiagram, self).color_to_rgba(color)
        if color[3] != 1.0:
            warnings.warn("Warning, trasparent colors are not supported", stacklevel=4)

        return color

    def clear(self):
        pass

    def _polygon_path(self, verts):
        path = pdb.gimp_vectors_new(self.image, "")

        v0 = verts[0]
        strokeid = pdb.gimp_vectors_bezier_stroke_new_moveto(path,
                                                             v0[0], v0[1])
        for v in verts[1:]:
            pdb.gimp_vectors_bezier_stroke_lineto(path, strokeid, v[0], v[1])

        pdb.gimp_vectors_stroke_close(path, strokeid)

        return path

    def _fill_path(self, path, fill_color):
        if fill_color:
            orig_foreground = pdb.gimp_context_get_foreground()
            orig_selection = pdb.gimp_selection_save(self.image)

            color = self.color_to_rgba(fill_color)
            pdb.gimp_context_set_foreground(color)

            pdb.gimp_image_select_item(self.image, CHANNEL_OP_REPLACE, path)

            pdb.gimp_edit_fill(self.drawable, FOREGROUND_FILL)

            pdb.gimp_selection_load(orig_selection)
            pdb.gimp_image_remove_channel(self.image, orig_selection)
            pdb.gimp_context_set_foreground(orig_foreground)

    def _stroke_path(self, path, stroke_color):
        if stroke_color:
            orig_paint_method = pdb.gimp_context_get_paint_method()
            orig_foreground = pdb.gimp_context_get_foreground()
            orig_brush = pdb.gimp_context_get_brush()

            pdb.gimp_context_set_paint_method('gimp-paintbrush')

            color = self.color_to_rgba(stroke_color)
            pdb.gimp_context_set_foreground(color)
            pdb.gimp_context_set_brush("1. Pixel")
            pdb.gimp_context_set_brush_size(self.stroke_width)

            pdb.gimp_edit_stroke_vectors(self.drawable, path)

            pdb.gimp_context_set_brush(orig_brush)
            pdb.gimp_context_set_foreground(orig_foreground)
            pdb.gimp_context_set_paint_method(orig_paint_method)

    def draw_star_by_verts(self, cx, cy, verts, stroke_color=(0, 0, 0)):
        path = pdb.gimp_vectors_new(self.image, "")

        for v in verts:
            strokeid = pdb.gimp_vectors_bezier_stroke_new_moveto(path, cx, cy)
            pdb.gimp_vectors_bezier_stroke_lineto(path, strokeid, v[0], v[1])

        pdb.gimp_image_insert_vectors(self.image, path, None, -1)
        pdb.gimp_image_set_active_vectors(self.image, path)

        self._stroke_path(path, stroke_color)

        pdb.gimp_image_remove_vectors(self.image, path)

    def draw_polygon_by_verts(self, verts,
                              stroke_color=(0, 0, 0),
                              fill_color=None):
        path = self._polygon_path(verts)

        pdb.gimp_image_insert_vectors(self.image, path, None, -1)
        pdb.gimp_image_set_active_vectors(self.image, path)

        self._fill_path(path, fill_color)
        self._stroke_path(path, stroke_color)

        pdb.gimp_image_remove_vectors(self.image, path)

    def copy_polygon(self, src_drawable, verts, dest_drawable, matrix):
        # flatten the verts list to be accepted by gimp_image_select_polygon()
        segs = [coord for v in verts for coord in v]
        pdb.gimp_image_select_polygon(self.image, CHANNEL_OP_REPLACE, len(segs), segs)

        pdb.gimp_edit_copy(src_drawable)
        floating_layer = pdb.gimp_edit_paste(dest_drawable, FALSE)

        pdb.gimp_item_transform_matrix(floating_layer,
                                       matrix[0], matrix[1], matrix[2],
                                       matrix[3], matrix[4], matrix[5],
                                       matrix[6], matrix[7], matrix[8])

        pdb.gimp_floating_sel_anchor(floating_layer)

    def draw_centered_text(self, cx, cy, text, theta, color,
                           align_baseline=False,
                           bb_stroke_color=None,
                           bb_fill_color=None):
        font_name = "Georgia"

        width, height, ascent, descent = pdb.gimp_text_get_extents_fontname(text,
                                                                            self.font_size,
                                                                            PIXELS,
                                                                            font_name)

        tx = cx - width / 2.0
        ty = cy - height / 2.0

        floating_selection = pdb.gimp_text_fontname(self.image, self.drawable,
                                                    tx, ty, text, 0, True,
                                                    self.font_size,
                                                    PIXELS,
                                                    font_name)

        text_color = self.color_to_rgba(color)
        pdb.gimp_text_layer_set_color(floating_selection, text_color)

        pdb.gimp_item_transform_rotate(floating_selection, theta, FALSE, cx, cy)
        pdb.gimp_floating_sel_anchor(floating_selection)

        if align_baseline:
            warnings.warn("The align_baseline option has not been implemented yet.")

        if bb_stroke_color or bb_fill_color:
            warnings.warn("Drawing the bounding box has not been implemented yet.")
