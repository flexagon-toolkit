#!/usr/bin/env python
#
# A Diagram abstraction based on Cairo
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from math import cos, sin, pi
import cairo
try:
    from .diagram import Diagram
except ValueError:
    from diagram import Diagram


class CairoDiagram(Diagram):
    def __init__(self, width, height, **kwargs):
        super(CairoDiagram, self).__init__(width, height, **kwargs)

        self.surface = cairo.RecordingSurface(0, (0, 0, width, height))
        self.cr = cr = cairo.Context(self.surface)

        cr.select_font_face("Georgia", cairo.FONT_SLANT_NORMAL,
                            cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(self.font_size)

        cr.set_line_width(self.stroke_width)
        cr.set_line_join(cairo.LINE_JOIN_ROUND)

    def clear(self):
        cr = self.cr

        r, g, b, a = self.color_to_rgba(self.background)
        cr.set_source_rgba(r, g, b, a)
        cr.paint()

    def save_svg(self, filename):
        surface = cairo.SVGSurface(filename + '.svg', self.width, self.height)
        # TODO: call surface.set_document_unit() to set units to pixels
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()

    def save_png(self, filename):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                     self.width, self.height)
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        surface.write_to_png(filename + '.png')

    def show(self):
        from PIL import Image
        from io import BytesIO
        f = BytesIO()
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                     self.width, self.height)
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        surface.write_to_png(f)
        f.seek(0)
        im = Image.open(f)
        im.show()

    def _draw_polygon(self, verts):
        cr = self.cr

        v = verts[0]
        cr.move_to(v[0], v[1])
        for v in verts[1:]:
            cr.line_to(v[0], v[1])
        cr.close_path()

    def _fill(self, fill_color, preserve=False):
        if fill_color:
            cr = self.cr
            r, g, b, a = self.color_to_rgba(fill_color)
            cr.set_source_rgba(r, g, b, a)
            if preserve:
                cr.fill_preserve()
            else:
                cr.fill()

    def _stroke(self, stroke_color, preserve=False):
        if stroke_color:
            cr = self.cr
            r, g, b, a = self.color_to_rgba(stroke_color)
            cr.set_source_rgba(r, g, b, a)
            if preserve:
                cr.stroke_preserve()
            else:
                cr.stroke()

    def draw_polygon_by_verts(self, verts,
                              stroke_color=(0, 0, 0),
                              fill_color=None):
        cr = self.cr

        cr.save()
        self._draw_polygon(verts)
        self._fill(fill_color, preserve=True)
        self._stroke(stroke_color)
        cr.restore()

    def draw_star_by_verts(self, cx, cy, verts, stroke_color=(0, 0, 0)):
        cr = self.cr

        for v in verts:
            cr.move_to(cx, cy)
            cr.line_to(v[0], v[1])

        self._stroke(stroke_color)

    def draw_circle(self, cx, cy, radius=10.0,
                    stroke_color=None,
                    fill_color=(0, 0, 0, 0.5)):
        cr = self.cr

        cr.save()

        cr.arc(cx, cy, radius, 0, 2 * pi)
        self._fill(fill_color, preserve=True)
        self._stroke(stroke_color)

        cr.restore()

    def draw_line(self, x1, y1, x2, y2, stroke_color=(0, 0, 0, 1)):
        cr = self.cr
        cr.move_to(x1, y1)
        cr.line_to(x2, y2)
        self._stroke(stroke_color)

    def draw_rect(self, x, y, width, height, theta=0,
                  stroke_color=None,
                  fill_color=(1, 1, 1, 0.8)):
        cr = self.cr

        cr.save()
        cr.translate(x, y)
        cr.rotate(theta)

        cr.rectangle(0, 0, width, height)
        self._fill(fill_color, preserve=True)
        self._stroke(stroke_color)

        cr.restore()

    def draw_centered_text(self, cx, cy, text, theta=0.0,
                           color=(0, 0, 0),
                           align_baseline=False,
                           bb_stroke_color=None,
                           bb_fill_color=None):
        cr = self.cr

        x_bearing, y_bearing, width, height, x_advance = cr.text_extents(text)[:5]
        ascent, descent = cr.font_extents()[:2]

        # The offset of the lower-left corner of the text.
        tx = width / 2.0 + x_bearing

        if align_baseline:
            # When aligning to the  baseline it is convenient the make the
            # bounding box depend on the font vertical extent and not from the
            # text content.
            ty = 0
            bb = [0, descent, width, -ascent]
        else:
            ty = height / 2.0 + y_bearing
            bb = [0, y_bearing, width, height]

        # The coordinate of the lower-left corner of the rotated rectangle
        rx = cx - tx * cos(theta) + ty * sin(theta)
        ry = cy - tx * sin(theta) - ty * cos(theta)

        cr.save()
        cr.translate(rx, ry)
        cr.rotate(theta)

        if bb_stroke_color or bb_fill_color:
            self.draw_rect(bb[0], bb[1], bb[2], bb[3], 0,
                           bb_stroke_color,
                           bb_fill_color)

        r, g, b, a = self.color_to_rgba(color)
        cr.set_source_rgba(r, g, b, a)

        cr.move_to(0, 0)
        cr.show_text(text)

        cr.restore()

        return x_advance


def test():
    diagram = CairoDiagram(400, 400)

    Diagram.test(diagram)

    diagram.show()
    diagram.save_svg('cairo_diagram_test.svg')


if __name__ == "__main__":
    test()
