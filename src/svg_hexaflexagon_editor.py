#!/usr/bin/env python3
#
# Draw an SVG hexaflexagon which can be edited live in Inkscape.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import svgwrite

from diagram.svgwrite_diagram import SvgwriteDiagram
from flexagon.hexaflexagon_diagram import HexaflexagonDiagram


class SvgwriteHexaflexagonDiagram(HexaflexagonDiagram):
    def __init__(self, *args, **kwargs):
        super(SvgwriteHexaflexagonDiagram, self).__init__(*args, **kwargs)

        svg = self.backend.svg

        # create some layers and groups
        layers = {
            "Hexagons": svg.layer(label="Hexagons"),
            "Backfaces": svg.layer(label="Backfaces"),
            "Hexaflexagon": svg.layer(label="Hexaflexagon"),
            "Folding guide": svg.layer(label="Folding guide"),
            "Template": svg.layer(label="Template")
        }
        for layer in layers.values():
            svg.add(layer)

        self.groups = layers

        for hexagon in self.hexaflexagon.hexagons:
            name = "hexagon%d-content" % hexagon.index
            layer = svg.layer(id=name, label="Hexagon %d" % (hexagon.index + 1))
            self.groups[name] = layer
            layers['Hexagons'].add(layer)

            for triangle in hexagon.triangles:
                name = "hexagon%d-triangle%d" % (hexagon.index, triangle.index)
                group = svg.g(id=name)
                self.groups[name] = group
                layers['Template'].add(group)

    def draw(self):
        for hexagon in self.hexaflexagon.hexagons:
            cx, cy = self.get_hexagon_center(hexagon)

            # Draw some default content
            old_active_group = self.backend.active_group
            self.backend.active_group = self.groups["hexagon%d-content" % hexagon.index]
            self.backend.draw_regular_polygon(cx, cy, 6, self.hexagon_radius,
                                              stroke_color=None,
                                              fill_color=(0.5, 0.5, 0.5, 0.2))
            self.backend.active_group = old_active_group

            # Add folding guides
            old_active_group = self.backend.active_group
            self.backend.active_group = self.groups["Folding guide"]

            for triangle in hexagon.triangles:
                cx, cy = self.get_triangle_center(triangle)
                theta = triangle.get_angle_in_hexagon()
                self.backend.draw_regular_polygon(cx, cy, 3, self.triangle_radius, theta,
                                                  stroke_color=(0, 0, 0, 0.2),
                                                  fill_color=None)
                polygon = self.backend.active_group.elements[-1]
                polygon['id'] = "hexagon%d-triangle%d-folding" % (triangle.hexagon.index,
                                                                  triangle.index)

            self.backend.active_group = old_active_group

        # Draw the normal template for hexagons
        for hexagon in self.hexaflexagon.hexagons:
            self.draw_hexagon_template(hexagon)

        # draw plan using references
        for hexagon in self.hexaflexagon.hexagons:
            for triangle in hexagon.triangles:
                m = self.get_triangle_transform(triangle)
                svg_matrix = "matrix(%f, %f, %f, %f, %f, %f)" % (m[0], m[3],
                                                                 m[1], m[4],
                                                                 m[2], m[5])

                # Reuse the hexagons triangle for the hexaflexagon template
                group = self.groups["Template"]
                triangle_href = "#hexagon%d-triangle%d" % (hexagon.index, triangle.index)
                ref = self.backend.svg.use(triangle_href)
                ref['transform'] = svg_matrix
                group.add(ref)

                # Reuse the folding guides
                group = self.groups["Folding guide"]
                folding_href = "#hexagon%d-triangle%d-folding" % (hexagon.index, triangle.index)
                ref = self.backend.svg.use(folding_href)
                ref['transform'] = svg_matrix
                group.add(ref)

                # Reuse the content to draw the final hexaflexagon
                group = self.groups["Hexaflexagon"]
                content_href = "#hexagon%d-content" % hexagon.index
                ref = self.backend.svg.use(content_href)
                ref['transform'] = svg_matrix
                ref['clip-path'] = "url(%s)" % (triangle_href + '-clip-path')
                group.add(ref)

        # draw the backfaces
        group = self.groups["Backfaces"]
        for hexagon in self.hexaflexagon.hexagons:
            for triangle in hexagon.triangles:
                m = self.get_triangle_backfaces_transform(triangle)
                svg_matrix = "matrix(%f, %f, %f, %f, %f, %f)" % (m[0], m[3],
                                                                 m[1], m[4],
                                                                 m[2], m[5])

                triangle_href = "#hexagon%d-triangle%d" % (hexagon.index, triangle.index)

                # Reuse the content to draw the backface
                content_href = "#hexagon%d-content" % hexagon.index
                ref = self.backend.svg.use(content_href)
                ref['transform'] = svg_matrix
                ref['clip-path'] = "url(%s)" % (triangle_href + '-clip-path')
                group.add(ref)

    def draw_triangle_template(self, triangle, cx, cy, theta):
        old_active_group = self.backend.active_group
        group_name = "hexagon%d-triangle%d" % (triangle.hexagon.index, triangle.index)
        self.backend.active_group = self.groups[group_name]

        super(SvgwriteHexaflexagonDiagram, self).draw_triangle_template(triangle, cx, cy, theta)

        # The triangle outline in the active group's element is the only polygon
        # element, so get it and set its id so that it can be reused as
        # a clip-path
        for element in self.backend.active_group.elements:
            if isinstance(element, svgwrite.shapes.Polygon):
                element['id'] = group_name + "-outline"
                break

        clip_path = self.backend.svg.clipPath(id=group_name + '-clip-path')
        self.backend.svg.defs.add(clip_path)
        ref = self.backend.svg.use('#%s-outline' % group_name)
        clip_path.add(ref)

        self.backend.active_group = old_active_group


def main():
    width = 3508
    height = 2480

    x_border = width / 50
    font_size = width / 80
    stroke_width = width / 480

    svg_backend = SvgwriteDiagram(width, height, font_size=font_size, stroke_width=stroke_width)
    hexaflexagon = SvgwriteHexaflexagonDiagram(x_border, backend=svg_backend)
    hexaflexagon.draw()
    svg_backend.save_svg("inkscape-hexaflexagon-editor.svg")


if __name__ == "__main__":
    main()
