#!/usr/bin/env python

'''
Gimp plugin "TestGimpDiagram"

Test the GImpDiagram class

Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from math import pi
import gimpfu
from gimpfu import *

from diagram.gimp_diagram import GimpDiagram

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)


def test_diagram_main(image):
    pdb.gimp_image_undo_group_start(image)
    pdb.gimp_context_push()
    pdb.gimp_context_set_defaults()

    template_layer_name = "TestGimpDiagram"
    template_layer = pdb.gimp_image_get_layer_by_name(image,
                                                      template_layer_name)
    if not template_layer:
        template_layer = pdb.gimp_layer_new(image, image.width, image.height,
                                            gimpfu.RGBA_IMAGE,
                                            template_layer_name,
                                            100,
                                            gimpfu.NORMAL_MODE)
        pdb.gimp_image_add_layer(image, template_layer, -1)

    diagram = GimpDiagram(image.width, image.height, image, template_layer,
                          font_size=10, stroke_size=2)

    diagram.draw_regular_polygon(200, 200, 6, 100, 0,
                                 fill_color=(1, 1, 0))

    diagram.draw_regular_polygon(200, 200, 6, 100, pi / 12.,
                                 stroke_color=(1, 0, 0, 0.2))

    diagram.draw_centered_text(200, 200, "__30__", pi / 6., (0, 0, 0))

    pdb.gimp_context_pop()
    pdb.gimp_image_undo_group_end(image)


if __name__ == "__main__":
    register(
        "python_fu_test_gimp_diagram",
        N_("Test GimpDiagram"),
        "Test GimpDiagram",
        "Antonio Ospite <ao2@ao2.it>",
        "Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>",
        "2017",
        N_("TestGImpDiagram..."),
        "RGB*, GRAY*",
        [
            (PF_IMAGE, "image", "Input image", None),
        ],
        [],
        test_diagram_main,
        menu="<Image>/Filters/Render",
        domain=("gimp20-python", gimp.locale_directory)
    )

    main()
