#!/usr/bin/env python
#
# A class to draw tetraflexagons
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .tritetraflexagon import TriTetraflexagon


class TetraflexagonDiagram(object):
    def __init__(self, x_border, backend=None):
        self.x_border = x_border
        self.backend = backend

        self.tetraflexagon = TriTetraflexagon()

        num_squares = len(self.tetraflexagon.squares)
        self.square_side = (self.backend.height - (x_border * 3)) / (num_squares)
        self.square_radius = self.square_side / 2
        self.tile_side = self.square_radius
        self.tile_radius = self.tile_side / 2

        self._init_centers()

        # draw the plan centered wrt. the squares
        self.plan_origin = ((self.backend.width - self.tile_side * 5) / 2,
                            self.x_border)

        self.backfaces_origin = (self.squares_centers[0][0] - self.tile_side,
                                 self.squares_centers[0][1] + self.x_border * 2 + self.tile_side)

        self.squares_color_map = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]

    def _init_centers(self):
        # Preallocate the lists to be able to access them by indices in the
        # loops below.
        self.squares_centers = [None for h in self.tetraflexagon.squares]
        self.tiles_centers = [[None for t in h.tiles] for h in self.tetraflexagon.squares]

        cy = self.backend.height - (self.square_radius + self.x_border)
        for square in self.tetraflexagon.squares:
            cx = self.x_border + (2 * self.square_radius + self.x_border) * (square.index + 1)
            self.squares_centers[square.index] = (cx, cy)

            for tile in square.tiles:
                # offset by 1 or -1 times the tile radius
                tile_xoffset, tile_yoffset = tile.calc_offset_in_square(self.tile_side)
                self.tiles_centers[square.index][tile.index] = (cx + tile_xoffset, cy + tile_yoffset)

    def get_square_center(self, square):
        return self.squares_centers[square.index]

    def get_tile_center(self, tile):
        return self.tiles_centers[tile.square.index][tile.index]

    def get_tile_center_in_plan(self, tile):
        x0, y0 = self.plan_origin
        i, j = self.tetraflexagon.get_tile_plan_position(tile)
        x, y = tile.calc_plan_coordinates(self.tile_side, i, j)
        return x0 + x, y0 + y

    def get_backface_tile_transform(self, tile):
        src_x, src_y = self.get_tile_center(tile)
        tile_xoffset, tile_yoffset = tile.calc_offset_in_square(self.tile_side)
        # When calculating dest_x the minus in the formula switches the columns.
        dest_x = self.backfaces_origin[0] + self.tile_side - tile_xoffset
        dest_y = self.backfaces_origin[1] + self.tile_side + tile_yoffset

        return self.backend.calc_rotate_translate_transform(src_x, src_y,
                                                            dest_x, dest_y, 0)

    def get_tile_transform(self, tile):
        """Calculate the transformation matrix from a tile in an square to
        the correspondent tile in the plan.

        Return the matrix as a list of values sorted in row-major order."""

        src_x, src_y = self.get_tile_center(tile)
        dest_x, dest_y = self.get_tile_center_in_plan(tile)

        i, j = self.tetraflexagon.get_tile_plan_position(tile)
        theta = tile.calc_angle_in_plan(i, j)

        return self.backend.calc_rotate_translate_transform(src_x, src_y,
                                                            dest_x, dest_y, theta)

    def draw_square_template(self, square):
        for tile in square.tiles:
            cx, cy = self.get_tile_center(tile)
            self.draw_tile_template(tile, cx, cy, 0)

    def draw_tile_template(self, tile, cx, cy, theta):
        side = self.tile_side
        color = self.squares_color_map[tile.square.index]

        self.backend.draw_rect_from_center(cx, cy, side, side, theta,
                                           stroke_color=color,
                                           fill_color=None)

        corners_labels = "ABC"
        corner_text = corners_labels[tile.square.index] + str(tile.index + 1)
        self.backend.draw_centered_text(cx, cy, corner_text, 0, color)

    def draw_plan_template(self):
        x0, y0 = self.plan_origin
        for square in self.tetraflexagon.squares:
            for tile in square.tiles:
                i, j = self.tetraflexagon.get_tile_plan_position(tile)
                x, y = tile.calc_plan_coordinates(self.tile_radius, i, j)
                theta = tile.get_angle_in_plan(i, j)
                self.draw_tile_template(tile, x0 + x, y0 + y, theta)

    def draw_template(self):
        for square in self.tetraflexagon.squares:
            self.draw_square_template(square)

        self.draw_plan_template()
