#!/usr/bin/env python
#
# A generic model for a tri-hexaflexagon
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from math import cos, sin, pi


class Triangle(object):
    def __init__(self, hexagon, index):
        self.hexagon = hexagon
        self.index = index

    @staticmethod
    def calc_plan_coordinates(radius, i, j):
        apothem = radius * cos(pi / 3.)
        side = 2. * radius * sin(pi / 3.)
        width = side
        height = apothem * 3.

        xoffset = (j + 1) * width / 2.
        yoffset = (i + (((i + j + 1) % 2) + 1) / 3.) * height

        return xoffset, yoffset

    def get_backface_index(self):
        # The backfaces have the triangles in the reverse rotational order
        return 5 - self.index

    def get_angle_in_plan(self):
        """The angle of a triangle in the hexaflexagon plan."""
        return - ((self.index) % 2) * pi / 3.

    def get_angle_in_plan_relative_to_hexagon(self):
        """"Get the angle of the triangle in the plan relative to the rotation
        of the same triangle in the hexagon."""
        # The explicit formula for this angle would be:
        #
        #    pi + pi / 6 + (((self.index + 1) % 6) // 2) * pi * 2 / 3
        #
        # The meaning of the part regarding the index is the following:
        #   - rotate the indices by 1
        #   - group by 2 (because couples of triangles move together in the
        #     plan)
        #   - multiply the group by a rotation factor
        #
        # The explicit formula shows clearly that triangles move in groups of
        # 2 in the plan.
        #
        # However, use an implicit form for robustness, so that if the other
        # angle functions change this one can be left untouched.
        return self.get_angle_in_hexagon() - self.get_angle_in_plan()

    def get_angle_in_backface_relative_to_hexagon(self):

        """"Get the angle of the triangle in the backface relative to the
        rotation of the same triangle in the hexagon."""

        backface_triangle_index = self.get_backface_index()
        # group triangles in couples
        group = (((backface_triangle_index + 1) % 6) // 2)
        return pi + pi * 2 / 3 * group

    def get_angle_in_hexagon(self):
        """Get the angle of the triangle in the hexagons.

        NOTE: the angle is rotated by pi to have the first triangle with the
        base on the bottom."""
        return pi + pi / 6. + self.index * pi / 3.

    def __str__(self):
        return "%d,%d" % (self.hexagon.index, self.index)


class Hexagon(object):
    def __init__(self, index):
        self.index = index
        self.triangles = []
        for i in range(6):
            triangle = Triangle(self, i)
            self.triangles.append(triangle)

    def __str__(self):
        output = ""
        for i in range(0, 6):
            output += str(self.triangles[i])
            output += "\t"

        return output


class TriHexaflexagon(object):
    def __init__(self):
        self.hexagons = []
        for i in range(0, 3):
            hexagon = Hexagon(i)
            self.hexagons.append(hexagon)

        # A plan is described by a mapping of the triangles in the hexagons,
        # repositioned on a 2d grid.
        #
        # In the map below, the grid has two rows, each element of the grid is
        # a pair (h, t), where 'h' is the index of the hexagon, and 't' is the
        # index of the triangle in that hexagon.
        plan_map = [
            [(0, 5), (1, 4), (1, 3), (2, 2), (2, 1), (0, 2), (0, 1), (1, 0), (1, 5)],
            [(2, 4), (2, 3), (0, 4), (0, 3), (1, 2), (1, 1), (2, 0), (2, 5), (0, 0)]
        ]

        # Preallocate a bi-dimensional array for an inverse mapping, this is
        # useful to retrieve the position in the plan given a triangle.
        self.plan_map_inv = [[-1 for t in h.triangles] for h in self.hexagons]

        self.plan = []
        for i, plan_map_row in enumerate(plan_map):
            plan_row = []
            for j, mapping in enumerate(plan_map_row):
                hexagon_index, triangle_index = mapping
                hexagon = self.hexagons[hexagon_index]
                triangle = hexagon.triangles[triangle_index]
                plan_row.append(triangle)

                self.plan_map_inv[hexagon_index][triangle_index] = (i, j)

            self.plan.append(plan_row)

    def get_triangle_plan_position(self, triangle):
        return self.plan_map_inv[triangle.hexagon.index][triangle.index]

    def __str__(self):
        output = ""

        for row in self.plan:
            for triangle in row:
                output += "%s\t" % str(triangle)
            output += "\n"

        return output


def test():
    trihexaflexagon = TriHexaflexagon()
    print(trihexaflexagon)


if __name__ == "__main__":
    test()
