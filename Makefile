all:

install_gimp_plugin:
	ln -sf $(PWD)/src/gimp_hexaflexagon.py ~/.gimp-2.8/plug-ins/
	ln -sf $(PWD)/src/gimp_diagram_test.py ~/.gimp-2.8/plug-ins/

uninstall_gimp_plugin:
	rm -f ~/.gimp-2.8/plug-ins/gimp_hexaflexagon.py
	rm -f ~/.gimp-2.8/plug-ins/gimp_diagram_test.py

pep8:
	find . -name "*.py" -print0 | xargs -r0 pep8 --ignore=E501

clean:
	find . -name "*.pyc" -print0 | xargs -r0 rm
	find . -type d -name "__pycache__" -print0 | xargs -r0 rm -r
