# flexagon-toolkit

flexagon-toolkit is a collection of tools to make it easier to build flexagons.


## Draw a template

To draw a template for a hexaflexagon execute:

```
$ ./src/cairo_hexaflexagon_template.py
```

And check out the generated `hexaflexagon-template.svg' file.


## Gimp plugin

To play with the GIMP plugin, install it with the following command:

```
$ make install_gimp_plugin 
```

Then launch Gimp and go to `Filters -> Render -> Hexaflexagon`.


## Live SVG flexagon editing in Inkscape

### Hexaflexagon

Create a new hexaflexagon base file with the following command:

```
$ ./src/svg_hexaflexagon_editor.py
```

Open the generated file `inkscape-hexaflexagon-editor.svg` in Inkscape and add
the hexagon content to the layers named *Hexagon 1*, *Hexagon 2*, *Hexagon 3*.

See the hexaflexagon being composed automatically and interactively.

### Tetraflexagon

Create a new tetraflexagon base file with the following command:

```
$ ./src/svg_tetraflexagon_editor.py
```

Open the generated file `inkscape-tetraflexagon-editor.svg` in Inkscape and add
the squares content to the layers named *Square 1*, *Square 2*, *Square 3*.

See the tetraflexagon being composed automatically and interactively.
